package com.test;

/**
 * 输入一个链表，输出该链表中倒数第k个结点。
 */
public class Test05 {
	static ListNode head=null;
    public static class ListNode{
    	int val;
    	ListNode next=null;
    	public ListNode(int val) {
    		this.val=val;
    	}
    	
        public static void   addNode(int val) {
        	ListNode node =new ListNode(val);
        	if(head==null) {
        		head=node;
        		return;
        	}
        	ListNode temp=head;
        	while(temp.next!=null) {
        		temp=temp.next;
        	}
        	temp.next=node;
        }	
    }	
    
    public ListNode FindKthToTail(ListNode head,int k) {
        if(head==null||k<0 ) return null;
        ListNode temp=head;
        int count=0;
        while(temp!=null){
        	count++;
        	temp=temp.next;
        }
        if(k>count)return null;
    	temp=head;
    	int i=0;
    	while(i<count-k) {
    		temp=temp.next;
    		i++;
    	}
 
        return temp;
    }
	
	public static void main(String[] args) {
        Test05 test=new Test05();
        for(int i=0;i<10;i++) {
        	ListNode.addNode(i);
        }
        
        ListNode temp=head;
        System.out.print(temp.val+" ");
        while(temp.next!=null) { 
        	temp=temp.next;
        	System.out.print(temp.val+" ");		
        }
        System.out.println();
        temp=test.FindKthToTail(head, 3);
	}
}
