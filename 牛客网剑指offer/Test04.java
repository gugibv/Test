package com.test;
/**
 * 输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有的奇数位于数组的前半部分，所有的偶数位于位于数组的后半部分，
 * 并保证奇数和奇数，偶数和偶数之间的相对位置不变
 */
public class Test04 {
	public void reOrderArray(int [] array) {
		 int []temp=new int[array.length];
		 int j=0;
         for(int i=0;i<array.length;i++) {
            if(array[i]%2!=0) {
            	temp[j++]=array[i];
            }
         }
         for(int x=0;x<array.length;x++) {
        	 if(array[x]%2==0) {
        		 temp[j++]=array[x];
        	 }
         }
         System.arraycopy(temp, 0, array, 0, temp.length);
    }
	public static void main(String[] args) {
		Test04 test=new Test04();
		int []arr= {1,2,3,4,5,6,7};
		test.reOrderArray(arr);
		for(int i:arr) {
			System.out.println(i);
		}
	}
}
