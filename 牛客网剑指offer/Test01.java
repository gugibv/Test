package com.test;

import java.util.ArrayList;

/**
 * 输入一个链表，从尾到头打印链表每个节点的值。
 */
public class Test01 {
	ListNode head = null;// 头结点

	/**
	 * 创建结点
	 */
	public static class ListNode {
		int val;// 存储当前结点值
		ListNode next = null;// 存储下个结点

		public ListNode(int val) {
			this.val = val;
		}
	}

	/**
	 * 添加结点
	 */
	public void addNode(int d) {
		ListNode node = new ListNode(d);
		if (head == null) {
			head = node;
			return;
		}

		ListNode temp = head;// 创建一个移动的指针
		while (temp.next != null) {
			temp = temp.next;
		}
		temp.next = node;
	}

	/**
	 * 将链表逆序
	 * 
	 * @param listNode
	 * @return
	 */
	public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
	    ListNode prev=null;
	    while(listNode!=null) {
	    	ListNode temp=listNode.next;
	    	listNode.next=prev;
	    	prev=listNode;
	    	listNode=temp;
	    }
		
	    
/*	   //理解二
		if(listNode==null) {
			return array;
		} 
		
		ListNode p=listNode;
	         ListNode q=listNode.next;
	         ListNode t=null;
	         while(q!=null) {
	                t=q.next;
	                q.next=p;
	                p=q;
	                q=t;
	         }
	         if(q==null) {
	        	 listNode.next=null;
	        	 listNode=p;
	         }*/

	    
		//将链表存储在动态数组里
		ArrayList<Integer> array = new ArrayList<Integer>();
		ListNode temp = null;
		temp = prev;
		while (temp != null) {
			array.add(temp.val);
			temp = temp.next;
		}
		return array;
	}

	public static void main(String[] args) {
		Test01 test = new Test01();
		int i = 0;
		while (i < 10) {
			test.addNode(i);
			i++;
		}
		ListNode temp = null;// 定义指针输出链表
		temp = test.head;
		while (temp != null) {
			System.out.print(temp.val + " ");
			temp = temp.next;
		}
		System.out.println();
		ArrayList<Integer> arrt = test.printListFromTailToHead(test.head);
		for (int j = 0; j < arrt.size(); j++) {
			System.err.print(arrt.get(j) + " ");
		}

	}

}
