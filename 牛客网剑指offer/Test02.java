package com.test;

/**
 * 输入某二叉树的前序遍历和中序遍历的结果，请重建出该二叉树。假设输入的前序遍历和中序遍历的结果中都不含重复的数字。
 * 例如输入前序遍历序列{1,2,4,7,3,5,6,8}和中序遍历序列{4,7,2,1,5,3,8,6}，则重建二叉树并返回。
 *
 */
public class Test02 {

	public TreeNode root = null;// 根节点

	/**
	 * 定义二叉树节点
	 */
	public class TreeNode {
		public int val;
		public TreeNode left = null;
		public TreeNode right = null;

		public TreeNode() {
		}

		public TreeNode(int val) {
			this.val = val;
		}
	}

	public void creteBinTree() {

	}

	public TreeNode reConstructBinaryTree(int[] pre, int[] in) {
		if (pre.length == 0 || in.length == 0 || pre.length != in.length) {
			return null;// 如果没有遍历，或者遍历不一致，则返回为null
		} 
		
		TreeNode root = new TreeNode(pre[0]);// 定义二叉树的根节点
		int i = 0;
		while (in[i] != pre[0]) {// 找到根节点再中序遍历里的位置，
			i++;
		}
		// 中序遍历中,根节点左侧为左子树节点，右侧为右子树节点
		int[] preLeft = new int[i];// 左子树先序遍历
		int[] inLeft = new int[i]; // 左子树中序遍历
		int[] preRight = new int[pre.length - i - 1];// 右子树先序遍历
		int[] inRight = new int[in.length - i - 1];// 右子树中序遍历
		
		for (int j = 0; j < in.length; j++) {
			if (j < i) {
				preLeft[j] = pre[j + 1];
				inLeft[j] = in[j];
			} else if (j > i) {
				preRight[j - i - 1] = pre[j];
				inRight[j - i - 1] = in[j];
			}
		}
		root.left = reConstructBinaryTree(preLeft, inLeft);// 递归构建左子树
		root.right = reConstructBinaryTree(preRight, inRight);// 递归构建右子树
		return root;// 返回重新构建二叉树
	}

	public static void last(TreeNode tr) {// 后序遍历遍历二叉树
		if (tr == null) {
			return;
		}
		last(tr.left);
		last(tr.right);
		System.out.print(tr.val + " ");
	}

	public static void main(String[] args) {
		int[] pre = { 1, 2, 4, 7, 3, 5, 6, 8 };
		int[] in = { 4, 7, 2, 1, 5, 3, 8, 6 };

		Test02 test02 = new Test02();
		TreeNode root=test02.reConstructBinaryTree(pre, in);
		last(root);

	}

}
