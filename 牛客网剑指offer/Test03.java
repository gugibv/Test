package com.test;

import java.util.Stack;

/**
 * 用两个栈来实现一个队列，完成队列的Push和Pop操作。 队列中的元素为int类型。
 *
 */
public class Test03 {
    Stack<Integer> stack1 = new Stack<Integer>();
    Stack<Integer> stack2 = new Stack<Integer>();
    
    public void push(int node) {
    	while(stack2.size()!=0) {
    		stack1.push(stack2.pop());
    	}
       stack1.push(node);
    }
    public int pop() {
        while(stack1.size()!=0) {
        	stack2.push(stack1.pop());
        }
      
        
    	return stack2.pop();
    }
    
    
	public static void main(String[] args) {
        Test03 test=new Test03();
        test.push(1);
        test.push(2);
        test.push(3);
        System.out.println(test.pop());
        System.out.println(test.pop());
        test.push(4);
        System.out.println(test.pop());
        test.push(5);
        System.out.println(test.pop());
        System.out.println(test.pop());
    
        
	}
}
